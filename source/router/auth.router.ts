import { Request, Response, Router } from "express";
import { json_web_tokens } from "../controllers/json_web_tokens";
import { User_Model } from "../models/user";
import { api_errors, auth_errors } from "../types/error_types";
export const AuthRouter = Router();
const jwt = new json_web_tokens();
/**
 * @swagger
 * tags:
 *  name: Auth
 *  description: Auth router for login, register and get the authenticated user information
 * 
 */

/**
 * @swagger
 * /api/auth/login:
 *  post:
 *     summary: This route can authenticate an user
 *     tags: [Auth]
 *     requestBody:
 *          content:
 *              application/json:
 *                  schema:
 *                      type: object
 *                      properties:
 *                          username:
 *                              type: string
 *                              description: UserNmae from user
 *                          password:
 *                              type: string
 *                              description: Password from user
 *                      example:
 *                          user: TestUser
 *                          password: TestPassword
 *     responses:
 *          200:
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              Token:
 *                                  type: string
 *                                  description: This is a token for the session, it's required to access to Protected Routes
 *          400:
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              Error:
 *                                  type: string
 *                              Description:
 *                                  type: string
 */
AuthRouter.post("/login", async (req: Request, res: Response) => {
    const { username, password }: { username: string, password: string } = req.body;
    if (!username || !password) return res.allFieldsError();

    const user = await User_Model.findOne({
        username: username
    });

    if (!user) return res.sendError({ code: 400, error: auth_errors.USER_DOESNT_EXISTS });
    const password_check = user.Decode(password);

    if (!password_check) res.sendError({ code: 400, error: auth_errors.PASSWORD_NOT_VALID });

    try {
        const token = jwt.encode({ _id: user.id });
        res.setHeader("X-TOKEN", token);

        res.json({
            Token: token
        })
    }catch(err){
        return res.sendError({
            code: 500,
            // @ts-ignore
            error: err
        })
    }
})

/**
 * @swagger
 * /api/auth/register:
 *  post:
 *     summary: This route can create a user
 *     tags: [Auth]
 *     requestBody:
 *          content:
 *              application/json:
 *                  schema:
 *                      type: object
 *                      properties:
 *                          username:
 *                              type: string
 *                              description: UserNmae from user
 *                          password:
 *                              type: string
 *                              description: Password from user
 *                          email:
 *                              type: string
 *                              description: Email for user
 *                          name:
 *                              type: string
 *                              description: Real name for user
 *                      example:
 *                          user: TestUser
 *                          password: TestPassword
 *                          name: John Cena
 *                          email: test@email.com
 *     responses:
 *          200:
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              Token:
 *                                  type: string
 *                                  description: This is a token for the session, it's required to access to Protected Routes
 *          400:
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              Error:
 *                                  type: string
 *                              Description:
 *                                  type: string
 */
AuthRouter.post("/register", async (req: Request, res: Response) => {
    const { username, password, email, name }: { username: string, password: string, email: string, name: string } = req.body;
    if (!username || !password || !email || !name) return res.allFieldsError();

    let user = await User_Model.findOne({
        username: username,
    });

    if (user) return res.sendError({
        code: 400,
        error: auth_errors.USERNAME_EXISTS
    });

    user = await User_Model.findOne({
        email: email,
    });

    if (user) return res.sendError({ code: 400, error: auth_errors.EMAIL_EXISTS });

    const user_for_save = new User_Model({
        email,
        name,
        username,

    });

    user_for_save.password = user_for_save.Encode(password);

    try {
        await user_for_save.save();
        const token = jwt.encode({
            _id: user_for_save.id
        });

        res.setHeader("X-AUTH", token);
        res.json({
            Token: token
        })
    } catch (e) {
        res.sendError({ code: 500, error: auth_errors.ERROR_CREATING_USER, data: String(e) });
    }
})

AuthRouter.get("/test", jwt.check.bind(jwt),  (req: Request, res: Response) => {
    console.log({user: req._user});
    res.json("All rigth");
})
import { Router } from "express";
import { WAPP_INSTANCE } from "../controllers/whatsapp.js";
import { api_errors, api_responses } from "../types/error_types";
import {json_web_tokens} from "../controllers/json_web_tokens";
import { Request, Response } from "express";

const jwt = new json_web_tokens()
export const WhatsApp_Router = Router();

let Instances_Create: { instance: WAPP_INSTANCE, id: string }[] = []
const wait = (ms:number): Promise<void> => new Promise(resolve => setTimeout(resolve, ms));

WhatsApp_Router.post("/create",jwt.check.bind(jwt), (req: Request, res: Response) => {
    const id = req._user.username;

    const f = Instances_Create.find(e => e.id === id);
    if (f) return res.sendError({
        code: 400,
        error: api_errors.HAS_WHATSAPP_CONNECTION_CREATED
    })

    console.log(`Opening instance: ${id}`);

    const instance = new WAPP_INSTANCE(id);
    Instances_Create.push({
        instance,
        id: id
    });

    console.log(Instances_Create);

    res.json({
        Message: api_responses.WHATSAPP_NEW_INSTANCE_OK
    })

})


WhatsApp_Router.post("/start", jwt.check.bind(jwt), (req: Request, res:Response) => {
    const id = req._user?.username;

    const heInstance = Instances_Create.find(e => e.id === id);
    if (!heInstance) return res.sendError({ code: 400, error: api_errors.NOT_WHATSAPP_INSTANCE })

    if (heInstance.instance.hasClient) return res.sendError({ code: 400, error: api_errors.HAS_WHATSAPP_CONNECTION_STARTED })
    heInstance.instance.open_instance()
        .then(e => {
            res.json(e)
        });

})

WhatsApp_Router.post("/run", jwt.check.bind(jwt), (req: Request, res: Response) => {
    console.log(req._user);
    
    const id = req._user?.username;

    let f = Instances_Create.find(e => e.id === id);
    if (f) return res.sendError({
        code: 400,
        error: api_errors.HAS_WHATSAPP_CONNECTION_CREATED
    })

    console.log(`Opening instance: ${id}`);

    const instance = new WAPP_INSTANCE(id);
    Instances_Create.push({
        instance,
        id
    });

    console.log(Instances_Create);

    f = Instances_Create.find(e => e.id === id);
    try{
        f?.instance.open_instance()
            .then(e => {
                return res.json(e)
            })
        console.log("TRYING");
        
    }
    catch(e){
        return res.status(500).json(e)
    }
})

WhatsApp_Router.post("/message", async (req: Request, res: Response) => {
    const {  message, phone }: { message: string, phone: string } = req.body;
    if (!message || !phone) return res.allFieldsError();
    
    const id = req._user?.username;

    const heInstance = Instances_Create.find(e => e.id === id);
    if (!heInstance) return res.sendError({
        code: 400,  error: api_errors.NOT_WHATSAPP_INSTANCE })

    if(!heInstance.instance.isLogged) return res.sendError({
        code: 400,  error: api_errors.NOT_WHATSAPP_AUTHENTICATED });


    try{
        await heInstance.instance.sendMessage(message, phone);
        return res.json({
            Message: api_responses.WHATSAPP_MESSAGE_OK
        })
    }catch(e){
        return res.status(500).json(e)
    }
})

WhatsApp_Router.post("/message/bulk", async (req: Request, res: Response) => {
    const { message, phone }: { message: string, phone: string[] } = req.body;
    if ( !message || !phone ) return res.allFieldsError();
    if (phone.length == 0) return res.allFieldsError();
    
    const id = req._user?.username;

    const heInstance = Instances_Create.find(e => e.id === id);
    if (!heInstance) return res.sendError({
        code: 400,  error: api_errors.IS_EMPTY_ARRAY })

    if(!heInstance.instance.isLogged) return res.sendError({
        code: 400,  error: api_errors.NOT_WHATSAPP_AUTHENTICATED });


    try{
        phone.forEach(async (e, i) => {
            e = e.replace(" ", "");
            // Por que 12
            // Se incluye minimo 1 simbolos para el codigo de país 1 por ejemplo cuenta como 1 simbolo
            // Se incluyen minimo 10 simbolos para el numero de telefono ej 1234567890
            // al final seran concatenados y quedara así 11234567890@s.whatsapp.net
            const currentPhone = `${e}@s.whatsapp.net`;
            await heInstance.instance.sendMessage(message, currentPhone);
            await wait(7000 + (i * 1000))
        })
        return res.json({
            Message: api_responses.WHATSAPP_MESSAGE_OK
        })
    }catch(e){
        return res.status(500).json(e)
    }  
})

WhatsApp_Router.get("/qr", jwt.check.bind(jwt), (req: Request, res: Response) => {
    const heInstance = Instances_Create.find(e => e.id === id);
    if (!heInstance) return res.sendError({
        code: 400,  error: api_errors.IS_EMPTY_ARRAY })
    const {QR_NAME} = heInstance.instance.QR 
})
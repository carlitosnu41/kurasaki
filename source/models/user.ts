import { Schema, model, Model } from "mongoose";
import { genSaltSync, hashSync, compareSync } from "bcrypt";

type UserModel_Type = Model<BD_USER, {}, BD_USER_METHODS>

export const UserSchema = new Schema({
    username: {
        type: String,
        required: true,
        unique: true
    },
    name: {
        type: String,
        required: true,
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true,
    },
    isUserActive: {
        type: Boolean,
        default: false
    }

})

export interface BD_USER {
    username: string,
    name: string,
    email: string,
    id: string,
    isUserActive?: boolean,
    password?: string,
}

export interface BD_USER_METHODS {
    Encode: (password: string) => string;
    Decode: (password: string) => boolean
}

UserSchema.method("Encode", (password: string) => {
    let salts = genSaltSync(10);
    return hashSync(password, salts);
})

UserSchema.method("Decode", (password: string) => {
    //@ts-ignore
    return compareSync(password, this?.password);
})

export const User_Model = model<BD_USER, UserModel_Type>("User", UserSchema);
import { connect } from "mongoose"

// @ts-ignore
export const connect_mongo = () => connect(process.env["CONNECTION_URL"]).then(e => console.log("Database is connected!"));
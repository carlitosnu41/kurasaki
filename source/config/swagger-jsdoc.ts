export const SwaggerJsDocConfiguration = {
    definition: {
        openapi: "3.0.0",
        info: {
            title: "Task API",
            version: "1.0.0",
            description: "Whatsapp Web JS API Documentation :D"
        },

        servers: [
            {
                url: "http://localhost:3000"
            }
        ],

    },
    apis: ["./source/router/*.ts"]
}
import {config} from "dotenv";
import {existsSync, mkdirSync} from "fs"
import {resolve} from "path"
config();

const enviroment_variables = [
    "CONNECTION_URL",
    "JWT_PASSWORD"
]
const QR_DIRECTORY = resolve("./QRs")
export function check(){
    console.log("⏰ Checking enviroment integrity before running...");
    
    for(let variable in enviroment_variables){
        const current_variable = enviroment_variables[variable];
        if(!process.env[current_variable]) throw new Error("Can't run without enviroment variable \"" + current_variable + "\"");
    }

    if(!existsSync(QR_DIRECTORY)){
        mkdirSync(QR_DIRECTORY);
        console.log("[✅ Fix]: Created QR Codes folder for prevent crashes")
    }
}
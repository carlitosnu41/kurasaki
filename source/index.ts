import express, { NextFunction, Request, Response } from "express";
import cors from "cors";
import morgan from "morgan";
import {check} from "./config/env.check"

// Check product integrity
check()

// Swagger is a module for autdocument an API without write it, all are in comment's :D
import SwaggerUI from "swagger-ui-express";
import SwaggerJsDoc from "swagger-jsdoc";
import { SwaggerJsDocConfiguration } from "./config/swagger-jsdoc";
import { AuthRouter } from "./router/auth.router";
import { connect_mongo } from "./mongo";
import { WhatsApp_Router } from "./router/whatsapp.router";
import { api_errors } from "./types/error_types";

connect_mongo()

// Creating app!
const app = express();

// Setting basic configuration
app.use(morgan("dev"));
app.use(cors({
    origin: "*"
}));

app.set("PORT", process.env.PORT || 3000);

// Middleware for acept json request!
app.use(express.json());
app.use(express.urlencoded({extended: false}));

// Middleware for extend request and response
app.use((req: Request, res:Response, next: NextFunction) => {
    res.sendError = (Error) => {
        res.status(Error.code).json({
            Error: Error.error,
            Details: Error.data
        });
    }


    res.allFieldsError = () => {
        res.status(400).json({
            Error: api_errors.ALL_FIELDS_ERROR
        });
    }
    next();
})

// Middleware for load swagger UI
const JsDocSpecs = SwaggerJsDoc(SwaggerJsDocConfiguration)
app.use("/docs", SwaggerUI.serve, SwaggerUI.setup(JsDocSpecs))

// Register routes
//                    FROM  Router
// structure app.use ( "/", MainRouter );
app.use("/api/auth", AuthRouter);
app.use("/api/whatsapp", WhatsApp_Router)
app.listen(app.get("PORT"), () => console.log(`App's listening on PORT: ${app.get("PORT")}!\nLet's develop`));
import { NextFunction, Request, Response } from "express";
import {sign, verify, Jwt} from "jsonwebtoken"
import { User_Model } from "../models/user";
import { auth_errors, error_send } from "../types/error_types";
interface token extends Jwt{
    _id: string
}
export class json_web_tokens{
    jwt_password = process.env["JWT_PASSWORD"];
    constructor(){

    }
    encode({_id}:{_id: string}): string{
        try{
            // @ts-ignore
            return sign({_id}, this.jwt_password, {
                expiresIn: "2d"
            })
        }catch(e){
            throw {
                log: e,
                error: auth_errors.JWT_ERROR
            };
        }
    }

    decode(token: string){
        if(!token) throw {Error: auth_errors.NOT_JWT_GIVEN}
        try{
            // @ts-ignore
            const decoded:token = verify(token, this.jwt_password); 
            if(!decoded._id) throw {Error: auth_errors.INVALID_JWT}

            return decoded._id
        }catch(err){
            throw {Error: auth_errors.JWT_VERIFICATION_ERROR}
        }
    }

    // Middleware para rutas protegidas
    async check(req: Request, res: Response, next: NextFunction){
        const token = req.headers["x-token"]
        //console.log(req.headers);
        
        if(!token) return error_send(res, 400, auth_errors.NOT_JWT_GIVEN);

        try{
            const decoded = this.decode(token.toString());
            const f_user  = await User_Model.findById(decoded).exec();
            if(!f_user) throw {
                Error: auth_errors.INVALID_JWT
            }

            req._user = {email: f_user.email, id: f_user.id, name: f_user.name, username: f_user.username,isUserActive:f_user.isUserActive, password: ""};
            
            next()
        }catch(e){
            console.log(e);
            return res.sendError({
                code: 400,
                // @ts-ignore
                error: typeof e === "object" ? e.Error : String(e)
            });
        }
    }
}
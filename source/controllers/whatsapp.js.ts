import {Client, LocalAuth} from "whatsapp-web.js";
import { api_errors, api_responses } from "../types/error_types";
import {toFile} from "qrcode"
import {existsSync, mkdirSync} from "fs"
import {resolve} from "path"
export class WAPP_INSTANCE{
    // @ts-ignore
    events_error: {
        log: string,
        Error: api_errors
    };
    QR = {
        directory: "./QRs",
        QR_NAME: "",
        full: resolve("./QRs")
    }
    instance_id: string;
    isLogged: Boolean = false;
    // @ts-ignore
    Client: Client;
    hasClient: Boolean = false;
    constructor(instance_id: string){
        this.instance_id  = instance_id;
        this.QR.QR_NAME = `QR_FROM_${instance_id}_.png`;
        this.QR.full = resolve(this.QR.directory, this.QR.QR_NAME);
    }
    async open_instance(): Promise<{Message: api_responses}> {
        try{
            this.Client = new Client({
                authStrategy: new LocalAuth({
                    clientId: this.instance_id
                })
            });

            await this.add_events.bind(this)();
            this.Client.initialize()
            this.hasClient = true

            return {
                Message: api_responses.WHATSAPP_CONNECTION_OK
            }
        }
        catch(error){
            throw error;
        }
    }
    async qr_generator(qr: string): Promise<{location?: string}> {
        try{
            console.log(`📲 QR code generated in ${this.QR.QR_NAME}`);
            
            await toFile(resolve(this.QR.full), qr);
            return {
                location: this.QR.QR_NAME
            };
        }catch(err){
            console.error(err);
            this.events_error = {
                Error: api_errors.EVENTS_ERROR,
                log: String(err)
            };

            return {}
        }
    }
    add_events() {
        try{
            console.log("Registring events from " + this.instance_id);
            this.Client.on("loading_screen", (percent, message) => {
                console.log(`💫 Whatsapp Web JS from ${this.instance_id} | Percentage: ${percent}%`);
            })

            this.Client.on("ready", () => console.log("💫 Whatsapp Web JS is ready!"))
            this.Client.on("authenticated", () => {
                console.log("💫 Whatsapp Web JS is authenticated!")
                this.isLogged = true
            })
            this.Client.on("qr", this.qr_generator.bind(this));
        }

        catch( err ){
            throw err;
        }
    }

    async sendMessage(message: string, number: string){
        try{
            await this.Client.sendMessage(number, message);
        }
        catch(err){
            throw {
                log: err,
                Error: api_errors.MESSAGE_SEND_ERROR
            }
        }
    }
}
import { Response } from "express";

export enum api_errors{
    WHATSAPP_CONNECTION = "Error al conectar a Whatsapp",
    HAS_WHATSAPP_CONNECTION_CREATED = "Ya hay una instancia de Whatsapp creada",
    HAS_WHATSAPP_CONNECTION_STARTED = "La instancia de Whatsapp ya se encuentra iniciada",
    NOT_WHATSAPP_INSTANCE = "No existe una instancia asociada a su usuario",
    EVENTS_ERROR = "Error al iniciar los eventos de su instancia, contacte a soporte",
    NOT_WHATSAPP_AUTHENTICATED = "Whatsapp no se encuentra autenticado",
    INTERNAL_ERROR = "Error interno, favor de contactar a soporte",
    MESSAGE_SEND_ERROR = "Error al enviar el mensaje",
    IS_EMPTY_ARRAY = "Se requieren al menos un campo para la completación de la petición",
    WHATSAPP_NUMBER_INVALID = "Numero de telefono invalido",
    QR_ERROR = "Error al generar el Código QR para su instancía",
    ALL_FIELDS_ERROR = "Se requieren todos los campos para hacer esta acción!"
}

export enum auth_errors{
    PASSWORD_NOT_VALID      = "La contraseña no es valida",
    USER_DOESNT_EXISTS      = "El usuario no existe",
    USER_ISNT_ACTIVED       = "El usuario esta dado de baja",
    EMAIL_EXISTS            = "El email ya existe!",
    USERNAME_EXISTS         = "El usuario ya existe!",
    ERROR_CREATING_USER     = "Error al crear el usuario",

    JWT_VERIFICATION_ERROR  = "Error al verificar su sesión!",
    NOT_JWT_GIVEN           = "No se ha proveeido su token de autenticación",
    INVALID_JWT             = "Token de autenticación invalido",
    JWT_ERROR               = "Error al crear el token de autenticación" 
}

export enum api_responses{
    WHATSAPP_CONNECTION_OK = "El API de WhatsApp se incio correctamente",
    WHATSAPP_NEW_INSTANCE_OK = "La nueva instancia de Whatsapp se ha iniciado correctamente!",
    WHATSAPP_MESSAGE_OK = "Mensaje enviado correctamente!"
}

export const allFields = (res: Response) => res.status(400).json({ Error: api_errors.ALL_FIELDS_ERROR });
export const error_send = (res: Response, error_code: number = 400, error_string: api_errors | auth_errors, definition?: string) => res.status(error_code).json({
    Error: error_string,
    Definition: definition
})
import { BD_USER } from "../../models/user";
import { api_errors, auth_errors } from "../error_types";

interface Error_Params{code: number, error: string, data?:any}
export { }

declare global {
    namespace Express {
        export interface Request {
            _user: BD_USER,
        }

        export interface Response{
            sendError: (Error: {code: number, error: api_errors | auth_errors, data?: any}) => void;
            allFieldsError: () => void;
        }
    }
}